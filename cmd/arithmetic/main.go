package main

import (
	"gitlab.com/m-test/arithmetic/pkg/api"
	"log"
	"net/http"
)

func main() {
	log.Println("Server is starting. http://localhost:8080")
	http.HandleFunc("/calculate", api.CalculateHandler)
	log.Fatal(http.ListenAndServe(":8080", nil)) //todo: get port from config
}
