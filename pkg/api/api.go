package api

import (
	"encoding/json"
	"gitlab.com/m-test/arithmetic/pkg/calculator"
	"log"
	"net/http"
)

type Response struct {
	Status  int `json:"status"`
	Message int `json:"result"`
}

type ErrorResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

func CalculateHandler(w http.ResponseWriter, r *http.Request) {
	userAccess := r.Header.Get("User-Access")
	if userAccess != "superuser" { //todo: set in config
		log.Println("Access denied")
		http.Error(w, "Access denied", http.StatusForbidden)
		return
	}

	expression := r.URL.Query().Get("expression")
	if expression == "" {
		log.Println("Expression not provided")
		http.Error(w, "Expression not provided", http.StatusBadRequest)
		return
	}

	if !calculator.IsValidExpression(expression) {
		log.Println("Invalid expression format")
		errorResponse := ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Invalid expression format",
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorResponse)
		return
	}

	result, err := calculator.EvaluateExpression(expression)
	if err != nil {
		log.Println("Invalid expression")
		errorResponse := ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Invalid expression",
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorResponse)
		return
	}

	response := Response{
		Status:  http.StatusOK,
		Message: result.Result,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
