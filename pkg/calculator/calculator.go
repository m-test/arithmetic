package calculator

import (
	"fmt"
	"strings"
)

type CalculationResult struct {
	Result int `json:"result"`
}

func IsValidExpression(expression string) bool {
	return true //todo: add validation
}

func EvaluateExpression(expression string) (CalculationResult, error) {
	elements := strings.Split(expression, "+")
	result := 0

	for _, element := range elements {
		parts := strings.Split(element, "-")

		num, err := parseNumber(parts[0])
		if err != nil {
			return CalculationResult{}, err
		}
		result += num

		for i := 1; i < len(parts); i++ {
			num, err := parseNumber(parts[i])
			if err != nil {
				return CalculationResult{}, err
			}
			result -= num
		}
	}

	return CalculationResult{Result: result}, nil
}

func parseNumber(s string) (int, error) {
	var num int
	_, err := fmt.Sscanf(s, "%d", &num)
	if err != nil {
		return 0, err
	}
	return num, nil
}
